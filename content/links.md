+++
date = "2016-07-01T11:28:25-03:00"
menu = "main"
title = "Links"
type = "About"
weight = -210
+++

### Links

Atendendo a pedidos deixamos aqui links para aprofundar estudos acerca dos
eixos, abordando teorias e tecnologias utilizadas na Infra Ágil.

#### 1. Eixo Automação

Teorias<br>
https://en.wikipedia.org/wiki/Infrastructure_as_Code
https://en.wikipedia.org/wiki/Software_configuration_management
https://en.wikipedia.org/wiki/Version_control
https://en.wikipedia.org/wiki/Cloud_computing
https://en.wikipedia.org/wiki/Virtualization
https://management30.com

Mark Burgess<br>
http://markburgess.org
https://en.wikipedia.org/wiki/Mark_Burgess_(computer_scientist)
http://www.iu.hio.no/~mark/papers/cfengine_history.pdf

Configuration Management <br>
https://en.wikipedia.org/wiki/CFEngine<br>
https://en.wikipedia.org/wiki/Puppet_(software)<br>
https://en.wikipedia.org/wiki/Chef_(software)<br>
https://en.wikipedia.org/wiki/Salt_(software)<br>

Orquestradores<br>
https://get.fabric.io<br>
http://capistranorb.com<br>
https://en.wikipedia.org/wiki/Ansible_(software)<br>
https://docs.puppet.com/mcollective/<br>

Provisionamento<br>
http://vagrantup.com<br>
https://www.packer.io<br>
http://www.ubuntu.com/cloud/juju<br>
https://theforeman.org<br>
https://www.terraform.io<br>

Distribuições com foco em containers<br>
http://www.projectatomic.io<br>
https://coreos.com<br>

Cluster de containers<br>
http://mesos.apache.org<br>
http://aurora.apache.org<br>
https://www.nomadproject.io<br>
http://kubernetes.io<br>

Ferramentas de nuvem (autoservico)<br>
https://pt.wikipedia.org/wiki/Openstack
https://en.wikipedia.org/wiki/Apache_CloudStack
https://en.wikipedia.org/wiki/OpenNebula

#### 2. Eixo Entrega

Teorias<br>
https://en.wikipedia.org/wiki/Continuous_delivery
https://en.wikipedia.org/wiki/Continuous_integration
https://en.wikipedia.org/wiki/Test-driven_development
https://en.wikipedia.org/wiki/Acceptance_test-driven_development
https://en.wikipedia.org/wiki/Behavior-driven_development
https://en.wikipedia.org/wiki/Load_testing
https://en.wikipedia.org/wiki/Stress_testing
https://en.wikipedia.org/wiki/Security_testing

SW Repo<br>
https://archiva.apache.org<br>
http://www.sonatype.org/nexus/<br>
https://www.jfrog.com/artifactory/<br>

VCS-SCM<br>
https://pt.wikipedia.org/wiki/Git<br>
https://gitlab.com<br>
https://github.com<br>
https://bitbucket.com<br>

Continuous Integration<br>
http://buildbot.net<br>
https://jenkins.io<br>

Integration Tests<br>
http://kitchen.ci<br>
https://about.gitlab.com/gitlab-ci/<br>
https://travis-ci.org<br>
https://www.go.cd<br>

Web Tests<br>
https://saucelabs.com<br>
http://www.seleniumhq.org<br>
https://www.browserstack.com<br>
http://casperjs.org<br>
http://phantomjs.org<br>
https://slimerjs.org<br>
http://www.webpagetest.org<br>

Testes de carga<br>
http://jmeter.apache.org<br>
https://www.blazemeter.com<br>

Puppet+Git (Deploy simples)<br>
https://github.com/puppetlabs/puppetlabs-vcsrepo

#### 3. Eixo Métricas

Logs Management Systems<br>
https://www.elastic.co/products/logstash<br>
http://www.fluentd.org<br>
https://flume.apache.org<br>
https://splunk.com<br>

Status services<br>
https://statuspage.io<br>
https://status.io<br>
https://cachethq.io<br>

Sites Metric Crash Report<br>
https://getsentry.com<br>
https://bugsnag.com<br>
https://raygun.com<br>

Incident Metrics<br>
https://www.pagerduty.com<br>
https://victorops.com<br>
https://bigpanda.io<br>

Sites Metric APM<br>
https://newrelic.com<br>
https://github.com/naver/pinpoint<br>
https://glowroot.org/<br>

Monitoring<br>
https://www.datadoghq.com<br>
http://www.logicmonitor.com<br>
https://www.zabbix.com<br>
https://www.nagios.org<br>

Graphite Stack<br>
https://collectd.org<br>
https://github.com/etsy/statsd<br>
http://graphiteapp.org<br>
http://grafana.org<br>

ELK Stack<br>
https://www.elastic.co/products/elasticsearch<br>
https://www.elastic.co/products/logstash<br>
https://www.elastic.co/products/kibana<br>

#### 4. Eixo Pessoas

https://en.wikipedia.org/wiki/Kanban
https://en.wikipedia.org/wiki/Coding_Dojo
https://en.wikipedia.org/wiki/Scrum_(software_development)
https://en.wikipedia.org/wiki/Hackerspace
https://pt.wikipedia.org/wiki/Hackathon
https://en.wikipedia.org/wiki/Gamification

Formação **OPs**<br>
http://www.opsschool.org<br>
https://www.codecademy.com<br>
https://www.codeschool.com<br>
