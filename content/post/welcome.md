+++
categories = ["core"]
date = "2016-06-30T21:54:54-03:00"
description = ""
draft = false
image = "/img/about-bg.jpg"
title = ""
tags = ["conceito"]
+++

Infra Ágil é essencialmente um guia de adoção de boas práticas a ser utilizada dentro do seu time de operação, partindo da visão de sysadmins conectados aos modelos ágeis.

A ideia principal é que este modelo auxilie times de operação a executar uma transição segura dentro de sua infra para um cenário mais eficiente  e autônomo.

Infra Ágil tem como fundamentos principais infraestrutura como código, automação, entrega, métricas e métodos ágeis para a integração de seus times.

Tentamos organizar o modelo de forma a facilitar o seu entendimento e a sua adoção.
