+++
date = "2016-07-01T11:28:25-03:00"
menu = "main"
title = "Alliance"
type = "About"
weight = -209
+++

### Alliance

Os membros alliance são pessoas que vão colaborar com o site e com o conteúdo do modelo.

#### Autores

Guto Carvalho (@gutocarvalho)<br>
Miguel Filho (@mciurcio)<br>

#### Core Members

Fernando Ike (@fernandoike)<br>
José Eufrásio (@coredump)<br>
Rafael Gomes (@gomex)<br>

#### Como se tornar contribuidor?

Os contribuidores são profissionais que:

1) Enviaram PRs com índice alto de aprovação<br>
2) Atuaram ou atuam em projetos reais de infra ágil<br>
3) Disseminam o assunto em palestras e eventos<br>
4) Disseminam o assunto em comunidades<br>
5) São referência no cenário DevOps<br>
6) São referência no cenário Infa Ágil<br>
7) São aprovados pelos Core Members e Autores.<br>

Se você preenche os requisitos de 1 a 6, abra uma issue no repositório do projeto
com uma mini-bio e solicitação de adesão ao grupo de contribuidores.
